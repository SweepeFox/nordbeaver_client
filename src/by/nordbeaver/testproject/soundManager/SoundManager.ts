import pixiSound from "pixi-sound";

export class SoundManager {
    private readonly volume: number = 1;
    private readonly rootPathToSounds: string = '../../../../../assets/sounds';
    private readonly soundsFormat = 'mp3';
    private readonly sounds: Map<Sound, string>;

    public constructor() {
        this.sounds = new Map<Sound, string>([
            [Sound.BackgroundMusic, 'background_music'],
            [Sound.ButtonSFX, 'button_sfx']
        ]);
        pixiSound.volumeAll = this.volume;
        this.registerSounds(this.sounds);
    }

    private registerSounds(sounds: Map<Sound, string>): void {
        for (const soundName of sounds.values()) {
            try {
                pixiSound.add(soundName, `${this.rootPathToSounds}/${soundName}.${this.soundsFormat}`);
            }
            catch (exception: any) {
                console.log(`Error while registering sounds. Check the format, it should be (${this.soundsFormat}). Path to file: ${this.rootPathToSounds}/${soundName}. 
                \nException: ${exception}`);
            }
        }
    }

    public play(sound: Sound): void {
        try {
            pixiSound.play(this.sounds.get(sound));
        }
        catch (exception) {
            console.log(`Failed to play sound. Exception: ${exception}`);
        }
    }

    public enable(value: boolean): void {
        pixiSound.volumeAll = value ? this.volume : 0;
    }
}

export enum Sound {
    BackgroundMusic,
    ButtonSFX
}