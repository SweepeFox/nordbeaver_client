import { Texture } from "pixi.js";
import { BaseResourcesPackage } from "../../../../resources/BaseResourcesPackage";

export class PreloaderResourcesPackage extends BaseResourcesPackage {
    public get bg_preloader(): Texture {
        return this.texture("assets/environment/bg_preloader.png");
    }

    public get mi_bunny(): Texture {
        return this.texture("assets/characters/mi_bunny.png");
    }

    protected registerResourcesForDownload(): void {
        this.add("assets/environment/bg_preloader.png");
        this.add("assets/characters/mi_bunny.png");
    }
}