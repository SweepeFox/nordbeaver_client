import { StartupConfig } from "../../../configs/StartupConfig";
import { ResourceManager } from "../../../resources/ResourceManager";
import { Viewable } from "../../../scene/viewable/Viewable";
import { NetworkClient } from "../../network/NetworkClient";
import { NetworkConfigLoader } from "../../network/NetworkConfigLoader";
import { GameSceneResourcePackage } from "../gameScene/resources/GameSceneResourcePackage";
import { PreloaderResourcesPackage } from "./resources/PreloaderResourcesPackage";
import { PreloaderSceneController } from "./controller/PreloaderSceneController";
import { PreloaderConfigModel } from "./model/PreloaderConfigModel";
import { PreloaderSceneView } from "./view/PreloaderSceneView";
import { GameSceneUiResourcePackage } from "../gameScene/resources/GameSceneUiResourcePackage";

export class PreloaderScene extends Viewable<PreloaderConfigModel>
{
    public constructor(resourceManager: ResourceManager, networkClient: NetworkClient, startupConfig: StartupConfig, preloaderResourcesPackage: PreloaderResourcesPackage) {
        super();
        const controller = new PreloaderSceneController();
        this.view = new PreloaderSceneView(controller, resourceManager, networkClient, startupConfig, this.operationControls, preloaderResourcesPackage);
    }

    public static async build(): Promise<PreloaderScene> {
        const startupConfig = new StartupConfig();
        const resourceManager = new ResourceManager();
        const networkConfigLoader = new NetworkConfigLoader();
        resourceManager.registerResourcePackage(new PreloaderResourcesPackage());
        resourceManager.registerResourcePackage(new GameSceneResourcePackage());
        resourceManager.registerResourcePackage(new GameSceneUiResourcePackage());
        const preloaderResourcesPackage = await resourceManager.getResourcePackage(PreloaderResourcesPackage);
        const networkConfig = await networkConfigLoader.load();
        const networkClient = new NetworkClient(networkConfig, startupConfig.roomId);
        await networkClient.connect();
        return Promise.resolve(new PreloaderScene(resourceManager, networkClient, startupConfig, preloaderResourcesPackage));
    }
}