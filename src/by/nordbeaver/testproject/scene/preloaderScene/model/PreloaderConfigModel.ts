import { StartupConfig } from "../../../../configs/StartupConfig";
import { NetworkClient } from "../../../network/NetworkClient";
import { ResourcesConfigModel } from "./ResourcesConfigModel";

export class PreloaderConfigModel {
    public resources: ResourcesConfigModel;
    public networkClient: NetworkClient;
    public startupConfig: StartupConfig;
}