import { GameSceneResourcePackage } from "../../gameScene/resources/GameSceneResourcePackage";
import { GameSceneUiResourcePackage } from "../../gameScene/resources/GameSceneUiResourcePackage";

export class ResourcesConfigModel {
    public gameSceneResourcePackage: GameSceneResourcePackage;
    public gameSceneUiResourcePackage: GameSceneUiResourcePackage;
}