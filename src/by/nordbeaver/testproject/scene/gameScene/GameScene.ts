import { Viewable } from "../../../scene/viewable/Viewable";
import { DefaultGameView } from "./view/DefaultGameView";
import { GameSceneResourcePackage } from "./resources/GameSceneResourcePackage";
import { NetworkClient } from "../../network/NetworkClient";
import { SceneManager } from "../../../scene/SceneManager";
import { GameSceneUiResourcePackage } from "./resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../soundManager/SoundManager";
import { GameSceneViewController } from "./controller/GameSceneViewController";
import { PlayerModel } from "./components/player/model/PlayerModel";
import { GameStateModel } from "../../rootModel/models/GameStateModel";
import { StartupConfig } from "../../../configs/StartupConfig";

export class GameScene extends Viewable<void>
{
    private constructor(gameSceneResourcePackage: GameSceneResourcePackage, gameSceneUiResourcePackage: GameSceneUiResourcePackage, networkClient: NetworkClient, sceneManager: SceneManager, soundManager: SoundManager, playerModel: PlayerModel, gameStateModel: GameStateModel, startupConfig: StartupConfig) {
        super();
        const controller = new GameSceneViewController(gameSceneUiResourcePackage, gameSceneResourcePackage, sceneManager, soundManager, networkClient, playerModel, startupConfig);
        this.view = new DefaultGameView(controller, gameStateModel);
    }

    public static async build(gameSceneResourcePackage: GameSceneResourcePackage, gameSceneUiResourcePackage: GameSceneUiResourcePackage, networkClient: NetworkClient, sceneManager: SceneManager, soundManager: SoundManager, playerModel: PlayerModel, gameStateModel: GameStateModel, startupConfig: StartupConfig): Promise<GameScene> {
        return Promise.resolve(new GameScene(gameSceneResourcePackage, gameSceneUiResourcePackage, networkClient, sceneManager, soundManager, playerModel, gameStateModel, startupConfig));
    }
}