import { Texture } from "pixi.js";
import { BaseResourcesPackage } from "../../../../resources/BaseResourcesPackage";
export class GameSceneUiResourcePackage extends BaseResourcesPackage {
    public get coin_score_plate(): Texture {
        return this.texture("assets/ui/coin_score_plate.png");
    }

    public get collect_coin_icon(): Texture {
        return this.texture("assets/ui/collect_coin_icon.png");
    }

    public get btn_pause_active(): Texture {
        return this.texture("assets/ui/btn_pause_active.png");
    }

    public get btn_pause_hover(): Texture {
        return this.texture("assets/ui/btn_pause_hover.png");
    }

    public get btn_pause_press(): Texture {
        return this.texture("assets/ui/btn_pause_press.png");
    }

    public get btn_sound_1_active(): Texture {
        return this.texture("assets/ui/btn_sound_1_active.png");
    }

    public get btn_sound_1_hover(): Texture {
        return this.texture("assets/ui/btn_sound_1_hover.png");
    }

    public get btn_sound_1_press(): Texture {
        return this.texture("assets/ui/btn_sound_1_press.png");
    }

    public get btn_sound_0_active(): Texture {
        return this.texture("assets/ui/btn_sound_0_active.png");
    }

    public get btn_sound_0_hover(): Texture {
        return this.texture("assets/ui/btn_sound_0_hover.png");
    }

    public get btn_sound_0_press(): Texture {
        return this.texture("assets/ui/btn_sound_0_press.png");
    }

    public get btn_fullscreen_active(): Texture {
        return this.texture("assets/ui/btn_fullscreen_active.png");
    }

    public get btn_fullscreen_hover(): Texture {
        return this.texture("assets/ui/btn_fullscreen_hover.png");
    }

    public get btn_fullscreen_press(): Texture {
        return this.texture("assets/ui/btn_fullscreen_press.png");
    }

    public get info_plate_big(): Texture {
        return this.texture("assets/ui/info_plate_big.png");
    }

    public get header_info_plate(): Texture {
        return this.texture("assets/ui/header_info_plate.png");
    }

    public get leadboard_button_active(): Texture {
        return this.texture("assets/ui/leadboard_button_active.png");
    }

    public get leadboard_button_hover(): Texture {
        return this.texture("assets/ui/leadboard_button_hover.png");
    }

    public get leadboard_button_press(): Texture {
        return this.texture("assets/ui/leadboard_button_press.png");
    }

    public get play_button_active(): Texture {
        return this.texture("assets/ui/play_button_active.png");
    }

    public get play_button_hover(): Texture {
        return this.texture("assets/ui/play_button_hover.png");
    }

    public get play_button_press(): Texture {
        return this.texture("assets/ui/play_button_press.png");
    }

    public get user_name_bar(): Texture {
        return this.texture("assets/ui/user_name_bar.png");
    }

    public get login_button_active(): Texture {
        return this.texture("assets/ui/login_button_active.png");
    }

    public get login_button_hover(): Texture {
        return this.texture("assets/ui/login_button_hover.png");
    }

    public get login_button_press(): Texture {
        return this.texture("assets/ui/login_button_press.png");
    }

    public get ok_button_active(): Texture {
        return this.texture("assets/ui/ok_button_active.png");
    }

    public get ok_button_hover(): Texture {
        return this.texture("assets/ui/ok_button_hover.png");
    }

    public get ok_button_press(): Texture {
        return this.texture("assets/ui/ok_button_press.png");
    }

    public get collect_distance_icon(): Texture {
        return this.texture("assets/ui/collect_distance_icon.png");
    }

    public get arrow_btn_active(): Texture {
        return this.texture("assets/ui/arrow_btn_active.png");
    }

    public get arrow_btn_hover(): Texture {
        return this.texture("assets/ui/arrow_btn_hover.png");
    }

    public get arrow_btn_press(): Texture {
        return this.texture("assets/ui/arrow_btn_press.png");
    }

    public get place_1(): Texture {
        return this.texture("assets/ui/place_1.png");
    }

    public get place_2(): Texture {
        return this.texture("assets/ui/place_2.png");
    }

    public get place_3(): Texture {
        return this.texture("assets/ui/place_3.png");
    }

    public get highleader_scores_plate(): Texture {
        return this.texture("assets/ui/highleader_scores_plate.png");
    }

    public get midleader_name_plate(): Texture {
        return this.texture("assets/ui/midleader_name_plate.png");
    }

    public get midleader_scores_plate(): Texture {
        return this.texture("assets/ui/midleader_scores_plate.png");
    }

    protected registerResourcesForDownload(): void {
        this.add("assets/ui/coin_score_plate.png");
        this.add("assets/ui/collect_coin_icon.png");
        this.add("assets/ui/btn_pause_active.png");
        this.add("assets/ui/btn_pause_hover.png");
        this.add("assets/ui/btn_pause_press.png");
        this.add("assets/ui/btn_sound_1_active.png");
        this.add("assets/ui/btn_sound_1_hover.png");
        this.add("assets/ui/btn_sound_1_press.png");
        this.add("assets/ui/btn_fullscreen_active.png");
        this.add("assets/ui/btn_fullscreen_hover.png");
        this.add("assets/ui/btn_fullscreen_press.png");
        this.add("assets/ui/info_plate_big.png");
        this.add("assets/ui/header_info_plate.png");
        this.add("assets/ui/leadboard_button_active.png");
        this.add("assets/ui/leadboard_button_hover.png");
        this.add("assets/ui/leadboard_button_press.png");
        this.add("assets/ui/play_button_active.png");
        this.add("assets/ui/play_button_hover.png");
        this.add("assets/ui/play_button_press.png");
        this.add("assets/ui/user_name_bar.png");
        this.add("assets/ui/login_button_active.png");
        this.add("assets/ui/login_button_hover.png");
        this.add("assets/ui/login_button_press.png");
        this.add("assets/ui/ok_button_active.png");
        this.add("assets/ui/ok_button_hover.png");
        this.add("assets/ui/ok_button_press.png");
        this.add("assets/ui/btn_sound_0_active.png");
        this.add("assets/ui/btn_sound_0_hover.png");
        this.add("assets/ui/btn_sound_0_press.png");
        this.add("assets/ui/collect_distance_icon.png");
        this.add("assets/ui/arrow_btn_active.png");
        this.add("assets/ui/arrow_btn_hover.png");
        this.add("assets/ui/arrow_btn_press.png");
        this.add("assets/ui/place_1.png");
        this.add("assets/ui/place_2.png");
        this.add("assets/ui/place_3.png");
        this.add("assets/ui/highleader_scores_plate.png");
        this.add("assets/ui/midleader_name_plate.png");
        this.add("assets/ui/midleader_scores_plate.png");
    }
}