import { Texture } from "pixi.js";
import { BaseResourcesPackage } from "../../../../resources/BaseResourcesPackage";
export class GameSceneResourcePackage extends BaseResourcesPackage {
    public get bg_gradient(): Texture {
        return this.texture("assets/environment/bg_gradient.png");
    }

    public get mi_bunny(): Texture {
        return this.texture("assets/characters/mi_bunny.png");
    }

    public get floor(): Texture {
        return this.texture("assets/environment/floor.png");
    }

    protected registerResourcesForDownload(): void {
        this.add("assets/environment/bg_gradient.png");
        this.add("assets/characters/mi_bunny.png");
        this.add("assets/environment/floor.png");
    }
}