import MiniSignal from "mini-signals";
import { StartupConfig } from "../../../../configs/StartupConfig";
import { SceneManager } from "../../../../scene/SceneManager";
import { NetworkClient } from "../../../network/NetworkClient";
import { StartGamePopup } from "../../../popup/startGamePopup/StartGamePopup";
import { Sound, SoundManager } from "../../../soundManager/SoundManager";
import { Gamefield } from "../components/gamefield/Gamefield";
import { PlayerModel } from "../components/player/model/PlayerModel";
import { GameSceneResourcePackage } from "../resources/GameSceneResourcePackage";
import { GameSceneUiResourcePackage } from "../resources/GameSceneUiResourcePackage";
import { GameSceneUi } from "../ui/GameSceneUI";

export class GameSceneViewController {
    public readonly OnGameFinished: MiniSignal;
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly gameSceneResourcePackage: GameSceneResourcePackage;
    private readonly sceneManager: SceneManager;
    private readonly soundManager: SoundManager;
    private readonly networkClient: NetworkClient;
    private readonly playerModel: PlayerModel;
    private startGamePopup: StartGamePopup;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, gameSceneResourcePackage: GameSceneResourcePackage, sceneManager: SceneManager, soundManager: SoundManager, networkClient: NetworkClient, playerModel: PlayerModel, startupConfig: StartupConfig) {
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.gameSceneResourcePackage = gameSceneResourcePackage;
        this.sceneManager = sceneManager;
        this.soundManager = soundManager;
        this.networkClient = networkClient;
        this.playerModel = playerModel;
        networkClient.joinRoomRequest(startupConfig.userId, startupConfig.battleId);
        networkClient.readyRequest();
        soundManager.play(Sound.BackgroundMusic);
    }

    public createGameSceneUi(): GameSceneUi {
        return new GameSceneUi(this.gameSceneUiResourcePackage, this.soundManager, this.playerModel);
    }

    public createGamefield(): Gamefield {
        return new Gamefield(this.gameSceneResourcePackage, this.networkClient, this.playerModel);
    }

    private createStartGamePopup(): void {
        if (this.startGamePopup == null || this.startGamePopup == undefined) {
            this.startGamePopup = new StartGamePopup(this.gameSceneUiResourcePackage, this.sceneManager, this.soundManager, this.playerModel);
            this.sceneManager.showViewableAsync(this.startGamePopup);
        }
    }

    public onRoomStateWaitingForPlayers(): void {
    }

    public onRoomStateStartupCountdownStarted(): void {
    }

    public onRoomStateGameStarted(): void {
    }

    public onRoomStateGameFinished(): void {
    }

    public onRoomStateRewardsGranted(): void {
    }

    public onGameStateCreated(): void {
    }

    public onGameStateCountdownToStart(): void {
    }

    public onGameStateStarted(): void {
        this.createStartGamePopup();
    }

    public onGameStateFinished(): void {
    }

    public onGameResultReceived(): void {
        this.OnGameFinished.dispatch();
    }
}