import { MiniSignalBinding } from "mini-signals";
import { IDestroyOptions, Sprite } from "pixi.js";
import { NetworkClient } from "../../../../network/NetworkClient";
import { GameSceneResourcePackage } from "../../resources/GameSceneResourcePackage";
import { PlayerModel } from "./model/PlayerModel";
import { MovementController } from "./movement/MovementController";

export class Player extends Sprite {
    private readonly movementController: MovementController;
    private readonly onPositionUpdatedBinding: MiniSignalBinding;

    public constructor(gameSceneResourcePackage: GameSceneResourcePackage, networkClient: NetworkClient, playerModel: PlayerModel) {
        super(gameSceneResourcePackage.mi_bunny);
        this.movementController = new MovementController(networkClient, playerModel);
        this.onPositionUpdatedBinding = this.movementController.OnPositionUpdated.add(this.onPositionUpdated, this);
    }

    private onPositionUpdated(): void {
        console.log("position update");
    }

    public override destroy(options?: boolean | IDestroyOptions): void {
        super.destroy(options);
        this.movementController.destroy();
        this.movementController.OnPositionUpdated.detach(this.onPositionUpdatedBinding);
    }
}