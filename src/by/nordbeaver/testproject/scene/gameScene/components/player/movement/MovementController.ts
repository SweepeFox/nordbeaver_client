import MiniSignal, { MiniSignalBinding } from "mini-signals";
import { Input } from "../../../../../../input/Input";
import { UserInputMessage } from "../../../../../network/messages/UserInputMessage";
import { NetworkClient } from "../../../../../network/NetworkClient";
import { PlayerModel } from "../model/PlayerModel";

export class MovementController {
    public readonly OnPositionUpdated: MiniSignal;
    private readonly networkClient: NetworkClient;
    private readonly playerModel: PlayerModel;
    private readonly input: Input;
    private readonly onPlayerModelUpdateBinding: MiniSignalBinding;
    private readonly onInputUpdatedBinding: MiniSignalBinding;

    public constructor(networkClient: NetworkClient, playerModel: PlayerModel) {
        this.networkClient = networkClient;
        this.playerModel = playerModel;
        this.OnPositionUpdated = new MiniSignal();
        this.input = new Input();
        this.onInputUpdatedBinding = this.input.jumpSignal.add(this.sendUserInputMessage, this);
        this.onPlayerModelUpdateBinding = playerModel.onPlayerModelUpdateSignal.add(this.onPlayerModelUpdated, this);
    }

    private onPlayerModelUpdated(): void {
        this.notifyPositionUpdated();
    }
    
    private notifyPositionUpdated(): void {
        this.OnPositionUpdated.dispatch();
    }

    private sendUserInputMessage(): void {
        this.networkClient.send(new UserInputMessage());
    }

    public destroy(): void {
        this.input.jumpSignal.detach(this.onInputUpdatedBinding);
        this.playerModel.onPlayerModelUpdateSignal.detach(this.onPlayerModelUpdateBinding);
    }
}