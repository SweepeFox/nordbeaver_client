import { Container, Texture } from "pixi.js";
import { Button } from "../../../../../ui/widgets/Button";
import { Sound, SoundManager } from "../../../../soundManager/SoundManager";

export class ButtonWithSFX extends Button {
    private readonly soundManager: SoundManager;

    public constructor(parent: Container, up: Texture, over: Texture, down: Texture, soundManager: SoundManager) {
        super(parent, up, over, down);
        this.soundManager = soundManager;
    }

    protected override onPointerUp(): void {
        super.onPointerUp();
        this.soundManager.play(Sound.ButtonSFX);
    }
}