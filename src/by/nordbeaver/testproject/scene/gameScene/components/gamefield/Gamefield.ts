import { Container, Sprite } from "pixi.js";
import { Game } from "../../../../../Game";
import { NetworkClient } from "../../../../network/NetworkClient";
import { GameSceneResourcePackage } from "../../resources/GameSceneResourcePackage";
import { PlayerModel } from "../player/model/PlayerModel";
import { Player } from "../player/Player";

export class Gamefield extends Container {
    private readonly gameSceneResourcePackage: GameSceneResourcePackage;
    private readonly networkClient: NetworkClient;
    private readonly playerModel: PlayerModel;

    public constructor(gameSceneResourcePackage: GameSceneResourcePackage, networkClient: NetworkClient, playerModel: PlayerModel) {
        super();
        this.gameSceneResourcePackage = gameSceneResourcePackage;
        this.networkClient = networkClient;
        this.playerModel = playerModel;
        this.build();
    }

    private build(): void {
        const background = new Sprite(this.gameSceneResourcePackage.bg_gradient);
        background.width = Game.WIDTH;
        background.height = Game.HEIGHT;
        this.addChild(background);

        const floor = new Sprite(this.gameSceneResourcePackage.floor);
        floor.anchor.y = 1;
        floor.y = this.height;
        floor.width = background.width;
        this.addChild(floor);

        const player = new Player(this.gameSceneResourcePackage, this.networkClient, this.playerModel);
        player.scale.set(0.4, 0.4);
        player.anchor.y = 1;
        player.x = 300;
        player.y = floor.y - floor.height + 35;
        this.addChild(player);
    }
}