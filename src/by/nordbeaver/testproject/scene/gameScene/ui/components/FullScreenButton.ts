import { Container } from "pixi.js";
import { SoundManager } from "../../../../soundManager/SoundManager";
import { ButtonWithSFX } from "../../components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../resources/GameSceneUiResourcePackage";

export class FullScreenButton extends ButtonWithSFX {

    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.btn_fullscreen_active, gameSceneUiResourcePackage.btn_fullscreen_hover, gameSceneUiResourcePackage.btn_fullscreen_press, soundManager);
    }

    protected override onPointerUp(): void {
        super.onPointerUp();
        document.fullscreenElement ? this.closeFullscreen() : this.openFullscreen();
    }

    private openFullscreen(): void {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        }
    }

    private closeFullscreen(): void {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
}