import { Container } from "pixi.js";
import { ButtonWithSFX } from "../../../scene/gameScene/components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../../soundManager/SoundManager";

export class ArrowButton extends ButtonWithSFX {
    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.arrow_btn_active, gameSceneUiResourcePackage.arrow_btn_hover, gameSceneUiResourcePackage.arrow_btn_press, soundManager);
    }
}