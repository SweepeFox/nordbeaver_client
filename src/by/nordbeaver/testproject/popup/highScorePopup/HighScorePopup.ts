import { Sprite, Text, TextStyle } from "pixi.js";
import { Game } from "../../../Game";
import { SceneManager } from "../../../scene/SceneManager";
import { BaseView } from "../../../scene/viewable/BaseView";
import { Viewable, ViewableOperationControls } from "../../../scene/viewable/Viewable";
import { PointerEvent } from "../../../ui/events/PointerEvent";
import { PlayerModel } from "../../scene/gameScene/components/player/model/PlayerModel";
import { GameSceneUiResourcePackage } from "../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../soundManager/SoundManager";
import { StartGamePopup } from "../startGamePopup/StartGamePopup";
import { LeadboardSlider } from "./components/LeadboardSlider";
import { OkButton } from "./components/OkButton";

export class HighScorePopup extends Viewable<void> {
    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, sceneManager: SceneManager, soundManager: SoundManager, playerModel: PlayerModel) {
        super();
        const highScorePopupController = new HighScorePopupController(this.operationControls, sceneManager, gameSceneUiResourcePackage, soundManager, playerModel);
        this.view = new HighScorePopupView(gameSceneUiResourcePackage, highScorePopupController, soundManager,);
    }
}

class HighScorePopupController {
    private readonly operationControls: ViewableOperationControls<void>;
    private readonly sceneManager: SceneManager;
    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly soundManager: SoundManager;
    private readonly playerModel: PlayerModel;

    public constructor(operationControls: ViewableOperationControls<void>, sceneManager: SceneManager, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager, playerModel: PlayerModel) {
        this.operationControls = operationControls;
        this.sceneManager = sceneManager;
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.soundManager = soundManager;
        this.playerModel = playerModel;
    }

    public onOkButtonClick(): void {
        this.operationControls.complete();
        const startGamePopup = new StartGamePopup(this.gameSceneUiResourcePackage, this.sceneManager, this.soundManager, this.playerModel);
        this.sceneManager.showViewableAsync(startGamePopup);
    }
}

class HighScorePopupView extends BaseView {

    private readonly gameSceneUiResourcePackage: GameSceneUiResourcePackage;
    private readonly highScorePopupController: HighScorePopupController;
    private readonly soundManager: SoundManager;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, highScorePopupController: HighScorePopupController, soundManager: SoundManager) {
        super();
        this.gameSceneUiResourcePackage = gameSceneUiResourcePackage;
        this.highScorePopupController = highScorePopupController;
        this.soundManager = soundManager;
        this.pivot.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.position.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.start();
    }

    public start(): void {
        const frame = new Sprite(this.gameSceneUiResourcePackage.info_plate_big);
        frame.anchor.set(0.5, 0.5);
        frame.position.set(Game.WIDTH / 2, Game.HEIGHT / 2);
        this.addChild(frame);

        const header = new Sprite(this.gameSceneUiResourcePackage.header_info_plate);
        header.anchor.set(0.5, 0);
        header.y = -frame.height / 2;
        frame.addChild(header);

        const headerText = new Text("Таблица рекордов:", new HighScorePopupHeaderTextStyle());
        headerText.anchor.set(0.5, 0.5);
        headerText.y = header.height / 2;
        header.addChild(headerText);
        
        const leadboardSlider = new LeadboardSlider(this.gameSceneUiResourcePackage, this.soundManager);
        leadboardSlider.y = -frame.height / 2 + 100;
        frame.addChild(leadboardSlider);

        const okButton = new OkButton(frame, this.gameSceneUiResourcePackage, this.soundManager);
        okButton.anchor.set(0.5, 1);
        okButton.y = frame.height / 2 - 20;
        okButton.on(PointerEvent.Up, this.highScorePopupController.onOkButtonClick, this.highScorePopupController);
    }
}

class HighScorePopupHeaderTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 50,
            fill: 0x003d71
        });
    }
}