import { Text, TextStyle } from "pixi.js"

export class CoinsLabel extends Text {
    public constructor() {
        super("4", new CoinsLabelTextStyle());
    }
}

class CoinsLabelTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 90,
            fill: 0xf4ad25,
            dropShadow: true,
            dropShadowDistance: 5,
            dropShadowAlpha: 0.5
        });
    }
}