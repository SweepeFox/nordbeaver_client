import { Container } from "pixi.js";
import { ButtonWithSFX } from "../../../scene/gameScene/components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../../soundManager/SoundManager";

export class LoginButton extends ButtonWithSFX {
    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.login_button_active, gameSceneUiResourcePackage.login_button_hover, gameSceneUiResourcePackage.login_button_press, soundManager);
    }
}