import { Text, TextStyle } from "pixi.js"

export class RecordLabel extends Text {
    public constructor() {
        super("Рекорд: \n1234", new RecordLabelTextStyle());
    }
}

class RecordLabelTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 70,
            fill: 0x00fd17,
            dropShadow: true,
            dropShadowDistance: 5,
            dropShadowAlpha: 0.5
        });
    }
}