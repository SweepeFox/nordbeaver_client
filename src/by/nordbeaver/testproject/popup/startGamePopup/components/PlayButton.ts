import { Container } from "pixi.js";
import { ButtonWithSFX } from "../../../scene/gameScene/components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../../soundManager/SoundManager";

export class PlayButton extends ButtonWithSFX {
    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.play_button_active, gameSceneUiResourcePackage.play_button_hover, gameSceneUiResourcePackage.play_button_press, soundManager);
    }
}