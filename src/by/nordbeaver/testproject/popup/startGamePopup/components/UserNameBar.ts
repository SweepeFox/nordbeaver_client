import { MiniSignalBinding } from "mini-signals";
import { IDestroyOptions, Sprite, Text, TextStyle } from "pixi.js";
import { PlayerModel } from "../../../scene/gameScene/components/player/model/PlayerModel";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";

export class UserNameBar extends Sprite {
    private readonly playerModel: PlayerModel;
    private readonly playerModelUpdateBinding: MiniSignalBinding;
    private nameLabel: Text;

    public constructor(gameSceneUiResourcePackage: GameSceneUiResourcePackage, playerModel: PlayerModel) {
        super(gameSceneUiResourcePackage.user_name_bar);
        this.playerModel = playerModel;
        this.playerModelUpdateBinding = playerModel.onPlayerModelUpdateSignal.add(this.onPlayerModelUpdated, this);
        this.build();
    }

    private build(): void {
        this.nameLabel = new Text(this.playerModel.Nickname, new UserNameTextStyle());
        this.nameLabel.anchor.set(0, 0.5);
        this.nameLabel.x = -this.width / 2 + 30;
        this.nameLabel.y = this.height / 2;
        this.addChild(this.nameLabel);
    }

    private onPlayerModelUpdated(): void {
        this.nameLabel.text = this.playerModel.Nickname;
    }

    public override destroy(options?: boolean | IDestroyOptions): void {
        super.destroy(options);
        this.playerModel.onPlayerModelUpdateSignal.detach(this.playerModelUpdateBinding);
    }
}

class UserNameTextStyle extends TextStyle {
    public constructor() {
        super({
            align: "center",
            fontFamily: "Regular",
            fontSize: 50,
            fill: 0xFFFFFF,
        });
    }
}