import { Container } from "pixi.js";
import { ButtonWithSFX } from "../../../scene/gameScene/components/ui/ButtonWithSFX";
import { GameSceneUiResourcePackage } from "../../../scene/gameScene/resources/GameSceneUiResourcePackage";
import { SoundManager } from "../../../soundManager/SoundManager";

export class LeadboardButton extends ButtonWithSFX {
    public constructor(parent: Container, gameSceneUiResourcePackage: GameSceneUiResourcePackage, soundManager: SoundManager) {
        super(parent, gameSceneUiResourcePackage.leadboard_button_active, gameSceneUiResourcePackage.leadboard_button_hover, gameSceneUiResourcePackage.leadboard_button_press, soundManager);
    }
}