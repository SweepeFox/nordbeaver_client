import { DefaultMessage } from "./messages/DefaultMessage";
import { GameStateMessage } from "./messages/GameStateMessage";
import { GameConfigMessage } from "./messages/GameConfigMessage";
import { JoinRoomMessage } from "./messages/JoinRoomMessage";
import { UserInputMessage } from "./messages/UserInputMessage";
import { ErrorMessage } from "./messages/ErrorMessage";
import { ReadyToPlayMessage } from "./messages/ReadyToPlayMessage";
import { RoomStateMessage } from "./messages/RoomStateMessage";
import { DisconnectMessage } from "./messages/DisconnectMessage";
import { PongMessage } from "./messages/ping/PongMessage";
import { PingMessage } from "./messages/ping/PingMessage";

export class MessageTypeResolver
{
    private TypeCodeToTypeMap: Map<number, () => DefaultMessage> = new Map([
        [0, () => this.createInstance(DefaultMessage)],
        [1, () => this.createInstance(JoinRoomMessage)],
        [2, () => this.createInstance(GameStateMessage)],
        [3, () => this.createInstance(GameConfigMessage)],
        [4, () => this.createInstance(ErrorMessage)],
        [5, () => this.createInstance(UserInputMessage)],
        [6, () => this.createInstance(DisconnectMessage)],
        [7, () => this.createInstance(RoomStateMessage)],
        [8, () => this.createInstance(ReadyToPlayMessage)],
        [9, () => this.createInstance(PingMessage)],
        [10, () => this.createInstance(PongMessage)]
    ]);

    private TypeToTypeCodeMap: Map<string, number> = new Map([
        ['DefaultMessage', 0],
        ['JoinRoomMessage', 1],
        ['GameStateMessage', 2],
        ['GameConfigMessage', 3],
        ['ErrorMessage', 4],
        ['UserInputMessage', 5],
        ['DisconnectMessage', 6],
        ['RoomStateMessage', 7],
        ['ReadyToPlayMessage', 8],
        ['PingMessage', 9],
        ['PongMessage', 10]
    ]);

    public TypeToTypeCode(type: string): number
    {
        return this.TypeToTypeCodeMap.get(type);
    }

    public TypeCodeToType(typeCode: number): DefaultMessage
    {
        return this.TypeCodeToTypeMap.get(typeCode)();
    }

    private createInstance<T extends DefaultMessage>(ctr: {new (): T}): T {
        return new ctr();
    }
}