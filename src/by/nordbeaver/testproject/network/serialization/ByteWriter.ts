export class ByteWriter
{
    private buffer: Buffer;
    private index: number;

    public constructor(buffer: Buffer, startIndex: number = 0)
    {
        this.buffer = buffer;
        this.index = startIndex;
    }

    public static fromSize(size: number): ByteWriter
    {
        return new ByteWriter(Buffer.alloc(size), 0);
    }

    public WriteByte(v: number): void
    {
        this.buffer.writeUInt8(v, this.index++);
    }

    public WriteBool(v: boolean): void
    {
        if (v) this.buffer.writeUInt8(0x01, this.index++);
        else this.buffer.writeUInt8(0x00, this.index++);
    }

    public WriteInt(v: number): void
    {
        this.buffer.writeInt32LE(v, this.index);
        this.index += 4;
    }

    public WriteString(v: string): void
    {
        const strBytes = Buffer.from(v, 'utf8');
        const len = strBytes.length;
        this.WriteInt(len);
        for (let j = 0; j < len; j++)
        {
            this.buffer[this.index++] = strBytes[j];
        }
    }

    public WriteIntArray(v: Array<number>): void
    {
        const len = v.length;
        this.WriteInt(len);
        for (let i = 0; i < len; i++)
        {
            this.WriteInt(v[i]);
        }
    }

    public WriteByteArray(v: Uint8Array): void 
    {
        const len = v.length;
        this.WriteInt(len);
        for (let i = 0; i < len; i++)
        {
            this.WriteByte(v[i]);
        }
    }

    public WriteFloat(v: number): void
    {
        this.buffer.writeFloatLE(v, this.index);
        this.index += 4;
    }

    public GetBytes(): Buffer
    {
        return this.buffer;
    }
}