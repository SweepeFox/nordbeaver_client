export class ByteReader
{
    private buffer: Buffer;
    public get index(): number {
        return this._index;
    }
    private _index: number;

    public constructor(buffer: Buffer, startIndex: number = 0)
    {
        this.buffer = buffer;
        this._index = startIndex;
    }

    public ReadByte()
    {
        return this.buffer.readUInt8(this._index++);
    }

    public ReadBool(): boolean
    {
        return this.buffer.readUInt8(this._index++) > 0;
    }

    public ReadInt(): number
    {
        const offset = this.index;
        this._index += 4;
        return this.buffer.readInt32LE(offset);
    }

    public ReadFloat(): number {
        const offset = this.index;
        this._index += 4;
        return this.buffer.readFloatLE(offset);
    }

    public ReadString(): string
    {
        const len = this.ReadInt();
        const s = this.buffer.toString('utf8', this.index, this.index + len);
        this._index += len;
        return s;
    }

    public ReadIntArray()
    {
        const len = this.ReadInt();
        const ret = new Array<number>(len);
        for (let i = 0; i < len; i++)
        {
            ret[i] = this.ReadInt();
        }
        return ret;
    }

    public ReadByteArray()
    {
        const len = this.ReadInt();
        const ret = this.buffer.slice(this.index, this.index + len);
        this._index += len;
        return ret;
    }
}