import { ByteReader } from "../serialization/ByteReader";
import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class DisconnectMessage extends DefaultMessage 
{
    public reason: number;
    
    protected GetMessageSize(): number {
        return 1;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
        byteWriter.WriteByte(this.reason);
    }

    public FromBytes(bytes: Uint8Array): void {
        const reader:ByteReader = new ByteReader(Buffer.from(bytes), DefaultMessage.MESSAGE_HEADER_SIZE);
        this.reason = reader.ReadByte();
    }

}