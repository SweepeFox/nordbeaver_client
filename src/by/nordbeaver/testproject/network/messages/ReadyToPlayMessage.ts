import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class ReadyToPlayMessage extends DefaultMessage 
{
    protected GetMessageSize(): number {
        return 0;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
    }
}