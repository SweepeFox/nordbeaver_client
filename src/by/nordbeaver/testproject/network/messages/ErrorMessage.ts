import { ByteReader } from "../serialization/ByteReader";
import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class ErrorMessage extends DefaultMessage {
    public errorCode: number;

    protected GetMessageSize(): number {
        return 1;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
        byteWriter.WriteByte(this.errorCode);
    }

    public FromBytes(bytes: Uint8Array): void {
        const reader: ByteReader = new ByteReader(Buffer.from(bytes), DefaultMessage.MESSAGE_HEADER_SIZE);
        this.errorCode = reader.ReadByte();
    }
}

export enum ErrorMessageCode {
    GameAlreadyStarted,
    RoomIsFull,
    FailedToRejoinTheGame,
    FailedToSendMessageToRoom,
    FailedToAddNewRoom,
    FailToProcessDisconnectMessage,
    FailedToProcessFinishedRooms,
    RoomFinished,
    FailedToJoinRoom,
    FailedToStartGame,
    FailedToSendGroupMessage,
    GameFinished
}