import { MessageTypeResolver } from "../MessageTypeResolver";
import { ByteWriter } from "../serialization/ByteWriter";

export class DefaultMessage
{
    public static readonly MESSAGE_HEADER_SIZE: number = 4            //sizeof(int)  //Message length
                                            + 1             //sizeof(byte) //Message type
                                            + 4;            //sizeof(int); //Room Id
    public static readonly MESSAGE_LENGTH_POSITION: number = 0;
    public static readonly MESSAGE_TYPE_POSITION: number = 4;         //sizeof(int);
    public static readonly MESSAGE_ROOMID_POSITION: number = 4 + 1;   //sizeof(int) + sizeof(byte);

    protected GetMessageSize(): number {
        return 0;
    }
    
    protected WriteMessageBytes(byteWriter: ByteWriter): void {}

    public ToBytes(resolver: MessageTypeResolver, roomId: number): Buffer
    {
        const dataLength = this.GetMessageSize();
        const writer: ByteWriter = ByteWriter.fromSize(DefaultMessage.MESSAGE_HEADER_SIZE + dataLength);
        writer.WriteInt(dataLength);
        writer.WriteByte(resolver.TypeToTypeCode(this.getConstructorName()));
        writer.WriteInt(roomId);
        this.WriteMessageBytes(writer);
        return writer.GetBytes();
    }

    public FromBytes(bytes: Uint8Array): void {};
}