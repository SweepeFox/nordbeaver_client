import { ByteReader } from "../serialization/ByteReader";
import { ByteWriter } from "../serialization/ByteWriter";
import { DefaultMessage } from "./DefaultMessage";

export class GameStateMessage extends DefaultMessage {
    public player: Uint8Array;
    public gameState: number;
    public stateTime: number;

    protected GetMessageSize(): number {
        return 5 + this.player.length;
    }

    protected WriteMessageBytes(byteWriter: ByteWriter): void {
        byteWriter.WriteByteArray(this.player);
        byteWriter.WriteInt(this.gameState);
        byteWriter.WriteByte(this.stateTime);
    }

    public FromBytes(bytes: Uint8Array): void {
        const reader: ByteReader = new ByteReader(Buffer.from(bytes), DefaultMessage.MESSAGE_HEADER_SIZE);
        this.player = reader.ReadByteArray();
        this.gameState = reader.ReadInt();
        this.stateTime = reader.ReadByte();
    }

}