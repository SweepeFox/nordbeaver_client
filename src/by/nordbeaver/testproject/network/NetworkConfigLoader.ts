import { JsonLoader } from "../tools/JsonLoader";
import { NetworkConfig } from "./NetworkConfig";

export class NetworkConfigLoader {
    private readonly pathToConfig: string = "/configurations/networkConfig.json";

    public async load(): Promise<NetworkConfig> {
        try {
            return await JsonLoader.loadJson<NetworkConfig>(this.pathToConfig);
        }
        catch(e) {
            let reason = "unknown";
            if (e instanceof Error) {
                reason = e.message;
            }
            console.error(`Unable to load network config: ${reason}. Used default`);
            return new NetworkConfig();
        }
    }
}