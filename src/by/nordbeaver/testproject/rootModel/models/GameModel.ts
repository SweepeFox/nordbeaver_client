import MiniSignal from "mini-signals";
import { INetworkClient } from "../../network/INetwortClient";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { GameStateMessage } from "../../network/messages/GameStateMEssage";
import { GameState } from "./GameState";

export class GameModel {
    public readonly GameStateUpdated: MiniSignal = new MiniSignal();
    public get gameState(): GameState {
        return this.previousGameState;
    }
    private previousGameState: GameState;
    public get gameStateTime(): number {
        return this.previousGameStateTime;
    }
    private previousGameStateTime: number;

    public constructor(networkClient: INetworkClient) {
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage) {
        if (message instanceof GameStateMessage) {
            if (this.previousGameState != message.gameState) {
                this.previousGameState = message.gameState;
            }
            if (this.previousGameStateTime != message.stateTime) {
                this.previousGameStateTime = message.stateTime;
            }
            this.GameStateUpdated.dispatch();
        }
    }
}