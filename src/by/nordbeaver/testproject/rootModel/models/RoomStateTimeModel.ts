import MiniSignal from "mini-signals";
import { INetworkClient } from "../../network/INetwortClient";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { RoomStateMessage } from "../../network/messages/RoomStateMessage";

export class RoomStateTimeModel {
    private previousStateTime: number;
    public get roomStateTime(): number {
        return this.previousStateTime;
    }
    public readonly RoomStateTimeUpdated: MiniSignal;

    public constructor(networkClient: INetworkClient) {
        this.RoomStateTimeUpdated = new MiniSignal();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof RoomStateMessage) {
            if (this.previousStateTime != message.stateTimerSec) {
                this.previousStateTime = message.stateTimerSec;
                this.RoomStateTimeUpdated.dispatch();
            }
        }
    }
}