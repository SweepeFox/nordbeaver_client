import MiniSignal from "mini-signals";
import { INetworkClient } from "../../network/INetwortClient";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { RoomStateId, RoomStateMessage } from "../../network/messages/RoomStateMessage";

export class RoomStateModel {
    private previousState: RoomStateId;
    public get roomState(): RoomStateId {
        return this.previousState;
    }
    public readonly RoomStateUpdated: MiniSignal;

    public constructor(networkClient: INetworkClient) {
        this.RoomStateUpdated = new MiniSignal();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof RoomStateMessage) {
            if (this.previousState != message.state) {
                this.previousState = message.state;
            }
            this.RoomStateUpdated.dispatch();
        }
    }

}