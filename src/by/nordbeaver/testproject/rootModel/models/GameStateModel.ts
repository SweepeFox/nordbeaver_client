import MiniSignal from "mini-signals";
import { INetworkClient } from "../../network/INetwortClient";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { GameStateMessage } from "../../network/messages/GameStateMessage";
import { GameState } from "./GameState";

export class GameStateModel {
    private previousState: GameState;
    public get gameState(): GameState {
        return this.previousState;
    }
    public readonly GameStateUpdated: MiniSignal;

    public constructor(networkClient: INetworkClient) {
        this.GameStateUpdated = new MiniSignal();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof GameStateMessage) {
            if (this.previousState != message.gameState) {
                this.previousState = message.gameState;
            }
            this.GameStateUpdated.dispatch();
        }
    }
}