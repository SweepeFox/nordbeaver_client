import MiniSignal from "mini-signals";
import { DefaultMessage } from "../../network/messages/DefaultMessage";
import { GameConfigMessage } from "../../network/messages/GameConfigMessage";
import { NetworkClient } from "../../network/NetworkClient";
import { ByteReader } from "../../network/serialization/ByteReader";

export class GameConfigModel {
    public readonly GameConfigReceived: MiniSignal;
    public readonly records: Records;

    public constructor(networkClient: NetworkClient) {
        this.GameConfigReceived = new MiniSignal();
        this.records = new Records();
        networkClient.onMessage.add(this.onMessage, this);
    }

    private onMessage(roomId: number, message: DefaultMessage): void {
        if (message instanceof GameConfigMessage) {
            const allTimeRecordsReader: ByteReader = new ByteReader(Buffer.from(message.allTimeRecords));
            while (allTimeRecordsReader.index < message.allTimeRecords.length) {
                const record = new Record();
                record.nickname = allTimeRecordsReader.ReadString();
                record.score = allTimeRecordsReader.ReadInt();
                this.records.allTimeRecords.push(record);
            }
            const monthRecordsReader: ByteReader = new ByteReader(Buffer.from(message.monthRecords));
            while (monthRecordsReader.index < message.monthRecords.length) {
                const record = new Record();
                record.nickname = monthRecordsReader.ReadString();
                record.score = monthRecordsReader.ReadInt();
                this.records.monthRecords.push(record);
            }
            const weekRecordsReader: ByteReader = new ByteReader(Buffer.from(message.weekRecords));
            while (weekRecordsReader.index < message.weekRecords.length) {
                const record = new Record();
                record.nickname = weekRecordsReader.ReadString();
                record.score = weekRecordsReader.ReadInt();
                this.records.weekRecords.push(record);
            }
            this.GameConfigReceived.dispatch();
        }
    }
}

export class Record {
    public nickname: string;
    public score: number;
}

export class Records {
    public allTimeRecords: Record[];
    public monthRecords: Record[];
    public weekRecords: Record[];
}