import { NetworkClient } from "../network/NetworkClient";
import { PlayerModel } from "../scene/gameScene/components/player/model/PlayerModel";
import { ErrorModel } from "./models/ErrorModel";
import { GameConfigModel } from "./models/GameConfigModel";
import { GameStateModel } from "./models/GameStateModel";
import { GameStateTimeModel } from "./models/GameStateTimeModel";
import { RoomStateModel } from "./models/RoomStateModel";
import { RoomStateTimeModel } from "./models/RoomStateTimeModel";

export class RootModel {
    public readonly roomStateModel: RoomStateModel;
    public readonly roomStateTimeModel: RoomStateTimeModel;
    public readonly gameStateModel: GameStateModel;
    public readonly gameStateTimeModel: GameStateTimeModel;
    public readonly errorModel: ErrorModel;
    public readonly gameConfigModel: GameConfigModel;
    public readonly playerModel: PlayerModel;

    public constructor(networkClient: NetworkClient) {
        this.roomStateModel = new RoomStateModel(networkClient);
        this.roomStateTimeModel = new RoomStateTimeModel(networkClient);
        this.gameStateModel = new GameStateModel(networkClient);
        this.gameStateTimeModel = new GameStateTimeModel(networkClient);
        this.errorModel = new ErrorModel(networkClient);
        this.gameConfigModel = new GameConfigModel(networkClient);
        this.playerModel = new PlayerModel(networkClient);
    }
}