import { StageLayer } from "./StageLayer";

export class LoaderLayer extends StageLayer {
    public constructor() {
        super();
        this.interactive = false;
    }
}
