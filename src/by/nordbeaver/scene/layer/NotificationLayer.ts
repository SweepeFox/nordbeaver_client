import { StageLayer } from "./StageLayer";

export class NotificationLayer extends StageLayer {
    public constructor() {
        super();
        this.interactive = false;
    }
}
