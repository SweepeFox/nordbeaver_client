import { Container } from "@pixi/display";
import { Box } from "./Box";

export class VBox extends Box {
    private _childSpacing:number;

    public get childSpacing(): number {
        return this._childSpacing;
    }

    public set childSpacing(v: number) {
        this._childSpacing = v;
        this.dirty = true;
        this.alignChildren();
    }

    constructor(childSpacing: number, parent: Container) {
        super(parent);
        this._childSpacing = childSpacing;
    }

    public override alignmentStrategy() {
        let positionY = 0.0;
        for (let i = 0; i < this.children.length; i++) {
            let child = this.children[i];
            child.position.y = positionY;
            positionY += child.getLocalBounds().height + this.childSpacing;
        }
    }
}